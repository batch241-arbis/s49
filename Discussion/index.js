// Mock Database
// let posts = [];

// post ID
// let count = 1;

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));

// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading (default of behavior of submit)
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,		
			body: document.querySelector("#txt-body").value,
			userId: 1		
		}),
		headers: {"Content-type": "application/json; charset=UTF-8"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Successfully added!")
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	});
});

// RETRIEVE POST
const showPosts = (posts) => {
	// variable that will contain all the posts
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost('${post.id}')">Edit</button>
		<button onClick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// EDIT POST (Edit Button)
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
	// removeAttribute() - removes the attribute with the specified name from the element
	document.querySelector(`#btn-submit-update`).removeAttribute("disabled")
};


// UPDATE POST (Update Button)
document.querySelector('#form-edit-post').addEventListener('submit',(e) => {
	e.preventDefault();
/*	console.log("test")

	for(let i = 0; i < posts.length; i++){
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Post successfully updated!");
		}
	}*/
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {"Content-type": "application/json; charset=UTF-8"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Successfully updated")
		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;
		document.querySelector(`#btn-submit-update`).setAttribute("disabled", true)
	});
});

const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove();
	alert("Post successfully deleted");
}